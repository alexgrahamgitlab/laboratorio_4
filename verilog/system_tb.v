`timescale 1 ns / 1 ps

module system_tb;
	reg clk = 1;
	reg [20:0] counter = 0;
	always #5 clk = ~clk;
    integer fil;
	reg resetn = 0;
	initial begin
		fil = $fopen("/home/parallels/Desktop/prueba.txt","w");
		if ($test$plusargs("vcd")) begin
			$dumpfile("system.vcd");
			$dumpvars(0, system_tb);
		end
		repeat (2) @(posedge clk);
		resetn <= 1;
		$timeformat(-9,0," ns");
		
		
	end

	wire trap;
	wire [31:0] out_byte;
	wire out_byte_en;
	wire [3:0] R;
	wire [3:0] G;
	wire [3:0] B;
	wire HS;
	wire VS;
	wire clock;

	system uut (
		.clk        (clk        ),
		.resetn     (resetn     ),
		.trap       (trap       ),
		.out_byte   (out_byte   ),
		.out_byte_en(out_byte_en),
		.R          (R),
		.G          (G),
		.B          (B),
		.HS         (HS),
		.VS         (VS),
		.clock (clock)
	);

	always @(posedge clock) begin
		counter <= counter + 1; 
		if (counter<500000) begin
			//if (!fil)
			//$display("no se abrio");
			$timeformat(-9,0," ns");
			$fwrite(fil,"%t: %b %b %b %b %b\n",$time,HS, VS, R, G, B);
		end
		else begin
			//$fdisplay(fil, "test");	
			
		//end	
		//if (resetn)
			$fclose(fil);
			$finish;

		end
	end
endmodule
