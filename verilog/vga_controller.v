module VGA_controller (
	input clk,
	input resetn,
	input [11:0] color,
	input prom_flag,
	output reg [11:0] vga_out,
	output reg HOR_SYNC,
	output reg VERT_SYNC,
	output reg clock,
	output reg [19:0] add_color
    );

reg [15:0] HOR_COUNTER;
reg [31:0] VERT_COUNTER; 
reg [2:0]  divider_counter;
reg [20:0] pixel;
reg [20:0] linea; 
//reg 	   clock;
reg [3:0] prom;
	
	//Divisor de frecuencia de reloj.
	always @(posedge clk) begin
		if(~resetn) begin
			divider_counter <= 0;
			clock <= 0;
			HOR_COUNTER <= 0;
			VERT_COUNTER <= 0;
			linea <= 0;
			pixel <= 0;
		end else begin
			divider_counter <= divider_counter + 1;
			if(divider_counter == 1) begin
				divider_counter <= 0;
				clock = ~clock;
			end
		end
	end
	//Se utiliza el reloj de 25MHz
	always @(posedge clock) begin
		if(~resetn) begin
			
		end else begin
		    add_color <= linea+ pixel + (639*linea);
			if(HOR_COUNTER == 800) begin
				HOR_COUNTER <= 0;
				linea <= linea + 1;
				pixel<=0;
				//VERT_COUNTER <= VERT_COUNTER + 1;
			end else begin
				HOR_COUNTER = HOR_COUNTER + 1;
			end
			if (HOR_COUNTER > 160 && HOR_COUNTER < 800) begin
			    pixel <= pixel + 1;
			    if(pixel == 640) begin
			         pixel <= 0;
			    end
			end
			if(linea==480) begin
				    linea <= 0;
			end
			
			if(VERT_COUNTER == 449600) begin
				VERT_COUNTER <= 0;
				
			end else begin
				VERT_COUNTER = VERT_COUNTER + 1;
				
			end
		end
	end
	//Horizontal sync logic. 
	always @(HOR_COUNTER) begin
		HOR_SYNC =0;
		if (prom_flag) begin
			prom = (color[3:0]+color[7:4]+color[11:8])/3;
			vga_out[3:0] = prom;
			vga_out[7:4] = prom;
			vga_out[11:8] = prom;
		  end else begin 
			vga_out = color; 
			end
		//vga_out = color;
		//color = 'h0F0;
		
		if(HOR_COUNTER >= 16) begin
			HOR_SYNC = 0;
		end 
		if(HOR_COUNTER >= 112) begin
			HOR_SYNC = 1;
		end
		if(HOR_COUNTER > 160 && HOR_COUNTER < 800) begin
			if (prom_flag) begin
			prom = (color[3:0]+color[7:4]+color[11:8])/3;
			vga_out[3:0] = prom;
			vga_out[7:4] = prom;
			vga_out[11:8] = prom;
		  end else begin 
			vga_out = color;
			//PARA COLOR VERDE COMENTAR LINEA DE ARRIBA DESCOMENTAR LA DE ABAJO
			//vga_out = 'h0F0;
			end
		end

	end
	//Vertical sync logic. 
	always @(VERT_COUNTER) begin
		VERT_SYNC = 1;
		//color = 'h0F0;
		//vga_out = color;
		if(VERT_COUNTER >= 8000) begin
			VERT_SYNC = 0;
		end 
		if(VERT_COUNTER >= 9600) begin
			VERT_SYNC = 1;
		end
		if(VERT_COUNTER > 32800 && HOR_COUNTER < 521) begin
			if (prom_flag) begin
			prom = (color[3:0]+color[7:4]+color[11:8])/3;
			vga_out[3:0] = prom;
			vga_out[7:4] = prom;
			vga_out[11:8] = prom;
		  end else begin 
			vga_out = color; 
			end 
		end
	end

endmodule 