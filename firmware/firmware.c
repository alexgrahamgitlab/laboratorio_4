#include <stdint.h>


static void save_Color (uint32_t color, uint32_t pos) {
	//Se guardan el color, la posicion y se habilita la bandera de valido.
	*(( volatile uint32_t *)0x10000004 ) = pos;
	*(( volatile uint32_t *)0x10000000 ) = color;
	
	*(( volatile uint32_t *)0x10000008 ) = 1;
}
	
void draw_square(int x_centre, int y_centre, uint32_t color){
	int x = 0, y = 0, position = 0, x_position = 0, y_position = 0;		
	for (int y=0; y<60;y++){
		for (int x=0; x<63;x++){
		x_position = x + x_centre;
		y_position = y + y_centre;
		position = y_position + x_position + (639 * y_position);
		save_Color(color,position);
		}
	}
}	
void midPointcircle(int x_centre, int y_centre, int r, uint32_t rojo) {
	int x = r, y = 0, position = 0, x_position = 0, y_position = 0, x_position_f = 0;
	//print initial point of axes
	x_position_f = x + x_centre;
	y_position = y + y_centre;
	//position = y_position + x_position + (639 * y_position); 
	//save_Color(rojo,position);

	//print initial point of the other axes
	x_position = -x + x_centre;
	//y_position = y + y_centre;
	//position = y_position + x_position + (639 * y_position); 
	//save_Color(rojo,position);

	for (x_position, x_position < x_position_f , x_position++){
		position = y_position + x_position + (639 * y_position);
		save_Color(rojo,position);
	}

	x_position = x + x_centre;
	y_position = -y + y_centre;
	position = y_position + x_position + (639 * y_position); 
	save_Color(rojo,position);

	x_position = -x + x_centre;
	y_position = -y + y_centre;
	position = y_position + x_position + (639 * y_position); 
	save_Color(rojo,position);

	if (r > 0) 
    { 
        x_position = x + x_centre;
		y_position = -y + y_centre;
		position = y_position + x_position + (639 * y_position);
		save_Color(rojo,position);

		x_position = y + x_centre;
		y_position = x + y_centre;
		position = y_position + x_position + (639 * y_position);
		save_Color(rojo,position); 

		x_position = -y + x_centre;
		y_position = x + y_centre;
		position = y_position + x_position + (639 * y_position);
		save_Color(rojo,position); 
    } 
	int P = 1 - r;
	int x_position_i = 0, x_position_f = 0, y_position_i = 0, y_position_f = 0;
	while (x > y) {
		y++;

		//mid point inside or on perimeter
		if (P <= 0) {
			P = P + 2*y + 1;
		}

		//mid point is outside perimeter
		else {
			x--;
			P = P + 2*y - 2*x + 1;
		}

		//All the perimeter points have been printed
		if (x < y){
			break;
		}

		//Print all generated points and their reflections on the octants.
		
		x_position_f = x + x_centre;
		y_position = y + y_centre;
		//position = y_position + x_position + (639 * y_position);
		//save_Color(rojo,position); 

		x_position = -x + x_centre;
		//y_position = y + y_centre;
		//position = y_position + x_position + (639 * y_position);
		//save_Color(rojo,position);
		for (x_position, x_position < x_position_f , x_position++){
			position = y_position + x_position + (639 * y_position);
			save_Color(rojo,position);
		}
			 
		x_position_f = x + x_centre;
		y_position = -y + y_centre;
		//position = y_position + x_position + (639 * y_position);
		//save_Color(rojo,position); 

		x_position = -x + x_centre;
		//y_position = -y + y_centre;
		//position = y_position + x_position + (639 * y_position);
		//save_Color(rojo,position); 
		for (x_position; x_position < x_position_f ; x_position++){
			position = y_position + x_position + (639 * y_position);
			save_Color(rojo,position);
		}

		if (x != y) {
			x_position_f = y + x_centre;
			y_position = x + y_centre;
			//position = y_position + x_position + (639 * y_position);
			//save_Color(rojo,position);  

			x_position = -y + x_centre;
			//y_position = x + y_centre;
			//position = y_position + x_position + (639 * y_position);
			//save_Color(rojo,position); 
			for (x_position; x_position < x_position_f ; x_position++){
				position = y_position + x_position + (639 * y_position);
				save_Color(rojo,position);
			}
			
			x_position_f = y + x_centre;
			y_position = -x + y_centre;
			//position = y_position + x_position + (639 * y_position);
			//save_Color(rojo,position);
				
			x_position = -y + x_centre;
			//y_position = -x + y_centre;
			//position = y_position + x_position + (639 * y_position);
			//save_Color(rojo,position);
			for (x_position; x_position < x_position_f ; x_position++){
				position = y_position + x_position + (639 * y_position);
				save_Color(rojo,position);
			}            
		} 
	}

}
void main () {

	uint32_t rojo = 0xF00;
	uint32_t amarillo = 0xFF0;
	uint32_t azul = 0x006;
	uint32_t negro = 0x000;

	uint32_t position;
	uint32_t flag_half = 0;
	uint32_t width = 1;
	uint32_t counter;
	//while (1) {
		position = 0;
		//Se guardan mitades de la pantalla
		for(int i = 0; i < 480; i++) {
			for(int j = 0; j < 640; j++ ){
				//position = j*(1<<16)+i;
				position = i + j + (639 * i);
				//position = j + i;
				if(j < 319) {
					save_Color(amarillo, position);
				}
				else {
					save_Color(azul, position);
				}
				//save_Color(amarillo,position);
			}
		}
		
		//flag_half = 0;
		midPointcircle(319,279,60,rojo);
		*(( volatile uint32_t *)0x10000010 ) = 1;

	//}
}

